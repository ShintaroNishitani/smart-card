<?php
use Migrations\AbstractSeed;

/**
 * Customers seed.
 */
class CustomersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'shop_id' => '1',
                'no' => '0001',
                'name' => '患者A',
                'kana' => 'かんじゃA',
                'next_date' => '2020-10-17',
                'enable' => '1',
                'created' => '2020-10-20 15:52:07',
                'modified' => '2020-10-14 09:27:11',
            ],
            [
                'id' => '2',
                'shop_id' => '1',
                'no' => '0002',
                'name' => '患者B',
                'kana' => 'かんじゃB',
                'next_date' => NULL,
                'enable' => '1',
                'created' => '2020-10-13 10:55:02',
                'modified' => '2020-10-13 10:55:02',
            ],
            [
                'id' => '3',
                'shop_id' => '1',
                'no' => '0003',
                'name' => '患者C',
                'kana' => 'かんじゃC',
                'next_date' => '2020-11-01',
                'enable' => '1',
                'created' => '2020-10-13 16:08:39',
                'modified' => '2020-10-13 16:08:39',
            ],
        ];

        $table = $this->table('customers');
        $table->insert($data)->save();
    }
}
