<?php

namespace App\Model\Behavior;

use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\Utility\Text;
use Cake\ORM\TableRegistry;
use App\Controller\AppController;
use Cake\Log\Log;

/**
 * 共通コンポーネント
 */
class CommonBehavior extends Behavior {

    public $old_data;
    public $proc_flg;

    public function implementedEvents()
    {
        return [
            'Model.beforeFind' => 'beforeFind',
            'Model.beforeSave' => 'beforeSave',
            'Model.afterSave' => 'afterSave',
            'Model.beforeDelete' => 'beforeDelete',
            'Model.afterDelete' => 'afterDelete',
        ];
    }

    public function beforeFind(Event $event, Query $query, ArrayObject $options, $primary)
    {
		$query->where([$this->_table->getalias().'.enable' => 1]);
	    return $query;
    }

    public function beforeSave(Event $event, $entity, $options) {

    }

    public function beforeDelete(Event $event, $entity, $options) {

    }

    public function afterSave(Event $event, EntityInterface $entity, ArrayObject $options) {
    }

    public function afterDelete(Event $event, EntityInterface $entity, ArrayObject $options) {
    }

}
