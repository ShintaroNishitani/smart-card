<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\Routing\Router;


class CustomersController extends AppController
{

    /**
     * 患者一覧
     */
    public function index()
    {
        $this->set('title', __('<i class="nc-icon nc-badge"></i> 患者一覧'));

        $query = $this->Customers->find()->where(['Customers.shop_id'=>$this->Auth->user()['shop_id']]);
        $customers = $this->paginate($query);

        if ($this->request->is(['patch', 'post', 'put'])) {
            if (!$this->request->getData('id')) {
                $data = $this->Customers->newEntity();
            } else {
                $data = $this->Customers->get($this->request->getData('id'));
            }
            $data = $this->Customers->patchEntity($data, $this->request->getData());
            $data->shop_id = $this->Auth->user()['shop_id'];
            if ($this->Customers->save($data)) {
                $this->Flash->success(__('患者が登録されました。'));
                return $this->redirect(['action' => 'index']);

            } else {
                $this->log($data->errors());
                $this->Flash->error(__('患者の登録に失敗しました。'));
            }

        }

        $this->set(compact('customers'));
    }

    /**
     * 患者登録
     */
    public function edit($id = null)
    {
        $this->autoRender = false;

        $data = $this->Customers->newEntity();
        if ($id) {
            $data = $this->Customers->get($id);
            if ($data->next_date) {
                $data->next_date = $data->next_date->format('Y-m-d');
            }
            $this->log($data);
        }
        echo json_encode(compact('data'));
    }

    /**
     * 患者削除
     */
    public function delete($id = null)
    {
        $data = $this->Customers->get($id);
        $data->enable = 0;
        if ($this->Customers->save($data)) {
            $this->Flash->success(__('患者を削除しました。'));
        } else {
            $this->Flash->error(__('患者の削除に失敗しました。'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * 診察券
     */
    public function view($id)
    {
        $this->viewBuilder()->setLayout('none');

        $this->set('title', __('診察券'));

        $customer = $this->Customers->get($id, [
            'contain' => ['Shops'],
        ]);

        $url = Router::url('/', true) . "customers/view/". $id;

        $this->set(compact('customer', 'url'));
    }


}
