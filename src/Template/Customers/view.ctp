<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="apple-mobile-web-app-title" content="smart card">
<link rel="apple-touch-icon" href="/img/card_192.png">
<script>
    if('serviceWorker' in navigator){
        navigator.serviceWorker.register('/serviceworker.js').then(function(){
            console.log("Service Worker Registered");
        });
    }
</script>
<div class="row">
    <div class="col-lg-6">
        <div class="card card-pricing "  style="background-color:#f4f3ef;">
            <div class="card-header" style="padding:0px;margin:0px;">
                <h4 class="card-title">
                <i class="fa fa-address-card-o"></i> 診察券<br/>
                    <?= h($customer->shop->name) ?>
                </h4>
            </div>
            <div class="card-body">
                <?=$this->QrCode->text($url);?>
                <h5 class="card-title">カルテNo.<?= h($customer->no) ?></h5>
                <ul>
                    <li><?= h($customer->kana) ?></li>
                    <li><?= h($customer->name.'様') ?></li>
                    <li><?= h("初診日：".$customer->created->format('Y年m月d日')) ?></li>
                    <li><?= $customer->next_date?h("次回来院日：".$customer->next_date->format('Y年m月d日')):"" ?></li>
                </ul>
            </div>
            <hr>
            <div class="card-footer">
                <ul class="text-warning">
                    <li><i class="fa fa-phone"></i> <?= h($customer->shop->tel) ?></li>
                    <li><?= h("〒".$customer->shop->zip.$customer->shop->address1.$customer->shop->address2) ?></li>
                </ul>
                <ul class="text-danger text-left">
                    <li><?= nl2br($customer->shop->remark) ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>
