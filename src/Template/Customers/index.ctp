<script>
    function deleteData(id) {
        Swal.fire({
            title: '',
            text: "患者を削除しますか?",
            type: 'warning',
            showCancelButton: true,
            customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false,
            confirmButtonText: '削除',
            cancelButtonText: 'キャンセル'
        }).then((result) => {
            if (result.value) {
                location.href = "/customers/delete/" + id;
            }
        })
    }

    function editData(id) {
        $.ajax({
            type: "get",
            dataType: 'json',
            url: "/customers/edit/" + id,
            success : function(data){
                if (data && data.data){
                    console.log(data.data)
                    $('#id').val(data.data.id);
                    $('#no').val(data.data.no);
                    $('#name').val(data.data.name);
                    $('#kana').val(data.data.kana);
                    $('#next-date').val(data.data.next_date);

                    $('#addModal').modal()
                }
            }
        });
    }
</script>
<div class="row">
    <div class="col-xl-10 col-sm-12">
        <div class="card card-user">
            <div class="card-body">
                <button class="btn btn-outline-success btn-round" onclick="editData(0)"><span class="btn-label"><i class="fa fa-plus"></i></span>
                患者登録
                </button>
                <table class="table table-bordered table-striped text-nowrap">
                    <thead>
                        <tr>
                            <th class="text-center" scope="col" class="actions"><?= __('診察券') ?></th>
                            <th class="text-center" scope="col"><?= $this->Paginator->sort('No.') ?></th>
                            <th class="text-center" scope="col"><?= $this->Paginator->sort('名前') ?></th>
                            <th class="text-center" scope="col" class="actions"><?= __('') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($customers as $customer): ?>
                        <tr>
                            <td class="text-center">
                                <?= $this->Html->link('発行', ['action'=>'view', $customer->id], ['escape'=>false, "class"=>"btn btn-sm  btn-behance", 'target'=>'_blank']) ?>
                            </td>
                            <td><?= h($customer->no) ?></td>
                            <td><?= h($customer->kana) ?><br/><?= h($customer->name) ?></td>
                            <td class="text-center">
                                <button class="btn btn-sm btn-icon btn-round btn-success" onclick="editData(<?= h($customer->id) ?>)">
                                    <i class="fa fa-pencil"></i>
                                </button>
                                <button class="btn btn-sm btn-icon btn-round btn-reddit" onclick="deleteData(<?= h($customer->id) ?>)">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="paginator">
                    <ul class="pagination">
                        <?= $this->Paginator->first('<< ' . __('first')) ?>
                        <?= $this->Paginator->prev('< ' . __('前')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('次') . ' >') ?>
                        <?= $this->Paginator->last(__('last') . ' >>') ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-address-card-o" aria-hidden="true"></i> 患者登録</h5>
      </div>
      <?= $this->Form->create() ?>
          <?= $this->Form->hidden('id', ["id"=>"id"]);?>

        <div class="modal-body">
            <div class="form-group">
            <label>No.</label>
                <?= $this->Form->control('no', ["class"=>"form-control", "label"=>false]);?>
            </div>
            <div class="form-group">
                <label>名前</label>
                <?= $this->Form->control('name', ["class"=>"form-control", "label"=>false]);?>
            </div>
            <div class="form-group">
                <label>カナ</label>
                <?= $this->Form->control('kana', ["class"=>"form-control", "label"=>false]);?>
            </div>
            <div class="form-group">
                <label>次回来院日</label>
                <?= $this->Form->control('next_date', ["class"=>"form-control", "label"=>false]);?>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">閉じる</button>
          <?= $this->Form->button(__('登録'), ["type"=>"submit", "class"=>"btn btn-success"]) ?>
        </div>
      <?= $this->Form->end() ?>
    </div>
  </div>
</div>
